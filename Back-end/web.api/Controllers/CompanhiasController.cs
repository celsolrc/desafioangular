﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using web.api.Contexts;
using web.api.Models;

namespace web.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanhiasController : ControllerBase
    {
        private readonly MyDbContext _context;

        public CompanhiasController(MyDbContext context)
        {
            _context = context;
        }

        // GET: api/Companhias
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Companhia>>> GetCompanhias()
        {
            //return await _context.Companhias.ToListAsync();
            // return await _context.Companhias.Include("Voos").Include("Passagens").ToListAsync();

            return await _context.Companhias
                    .Include(companhia => companhia.Voos)
                        .ThenInclude(companhia => companhia.Passagens).ToListAsync();
        }

        // GET: api/Companhias/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Companhia>> GetCompanhia(int id)
        {
            //var companhia = await _context.Companhias.FindAsync(id);
            var companhia = await _context.Companhias.Include("Voos").FirstOrDefaultAsync(x => x.CompanhiaId == id);

            if (companhia == null)
            {
                return NotFound();
            }

            return companhia;
        }

        // PUT: api/Companhias/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompanhia(int id, Companhia companhia)
        {
            if (id != companhia.CompanhiaId)
            {
                return BadRequest();
            }

            _context.Entry(companhia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanhiaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Companhias
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Companhia>> PostCompanhia(Companhia companhia)
        {
            Companhia novaCompanhia = new Companhia();

            novaCompanhia.NomeCompanhia = companhia.NomeCompanhia;

            _context.Companhias.Add(novaCompanhia);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CompanhiaExists(novaCompanhia.CompanhiaId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCompanhia", new { id = companhia.CompanhiaId }, companhia);
        }

        // DELETE: api/Companhias/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Companhia>> DeleteCompanhia(int id)
        {
            var companhia = await _context.Companhias.FindAsync(id);
            if (companhia == null)
            {
                return NotFound();
            }

            _context.Companhias.Remove(companhia);
            await _context.SaveChangesAsync();

            return companhia;
        }

        private bool CompanhiaExists(int id)
        {
            return _context.Companhias.Any(e => e.CompanhiaId == id);
        }
    }
}
