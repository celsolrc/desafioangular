﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web.api.Models;

namespace web.api.Contexts
{
    public class MyDbContext : DbContext
    {

        public DbSet<Companhia> Companhias { get; set; }
        public DbSet<Voo> Voos { get; set; }
        public DbSet<Passagem> Passagens { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer("Data Source=localhost;Initial Catalog=Passagens;Persist Security Info=True; Integrated Security=True; User ID=agencialog; password=agencialog01");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Companhia>().ToTable("TB_Companhias");
            modelBuilder.Entity<Companhia>().HasKey(x => x.CompanhiaId);
            modelBuilder.Entity<Companhia>().Property(x => x.NomeCompanhia).HasMaxLength(20).IsRequired();

            modelBuilder.Entity<Voo>().ToTable("TB_Voos");
            modelBuilder.Entity<Voo>().HasKey(x => x.VooId);
            modelBuilder.Entity<Voo>().Property(x => x.CompanhiaId).IsRequired();
            modelBuilder.Entity<Voo>().Property(x => x.NumeroVoo).IsRequired();
            modelBuilder.Entity<Voo>().Property(x => x.DataPartida).IsRequired();
            modelBuilder.Entity<Voo>().Property(x => x.HoraPartida).IsRequired();

            modelBuilder.Entity<Passagem>().ToTable("TB_Passagem");
            modelBuilder.Entity<Passagem>().HasKey(x => x.PassagemId);
            modelBuilder.Entity<Passagem>().Property(x => x.CPF).HasMaxLength(15).IsRequired();
            modelBuilder.Entity<Passagem>().Property(x => x.Nome).HasMaxLength(20).IsRequired();
            modelBuilder.Entity<Passagem>().Property(x => x.Sobrenome).HasMaxLength(50).IsRequired();
            modelBuilder.Entity<Passagem>().Property(x => x.ValorPassagem).IsRequired();
            modelBuilder.Entity<Passagem>().Property(x => x.NumeroAssento).IsRequired();
            modelBuilder.Entity<Passagem>().Property(x => x.VooId).IsRequired();

            modelBuilder.Entity<Companhia>().HasData(new Companhia(1, "TAM"));
            modelBuilder.Entity<Companhia>().HasData(new Companhia(2, "GOL"));

            modelBuilder.Entity<Voo>().HasData(new Voo(1, 1, 192, new DateTime(2020, 10, 1), "09:00"));
            modelBuilder.Entity<Voo>().HasData(new Voo(2, 2, 460, new DateTime(2020, 9, 7), "13:00"));
            modelBuilder.Entity<Voo>().HasData(new Voo(3, 2, 359, new DateTime(2020, 8, 3), "17:00"));


            modelBuilder.Entity<Passagem>().HasData(new Passagem(1, "111.222.333-44", "Fulano", "de Tal", (float) 231.21, 10, 1));
            modelBuilder.Entity<Passagem>().HasData(new Passagem(2, "231.444.345-54", "Joao", "Silva", (float) 231.21, 13, 1));
            modelBuilder.Entity<Passagem>().HasData(new Passagem(3, "561.423.225-87", "Maria", "Silva", (float)231.21, 14, 1));

            modelBuilder.Entity<Passagem>().HasData(new Passagem(4, "445.222.333-44", "Beltrano", "Jose", (float)441.43, 10, 2));
            modelBuilder.Entity<Passagem>().HasData(new Passagem(5, "741.345.543-21", "Alberto", "Cabral", (float)441.43, 13, 2));

            modelBuilder.Entity<Passagem>().HasData(new Passagem(6, "334.562.542-39", "Jose", "Coves", (float)345.78, 14, 3));

            base.OnModelCreating(modelBuilder);
        }


    }
}
