﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace web.api.Models
{
    public class Passagem
    {
        public Passagem() { }
        public Passagem( int passagemId, string cpf, string nome, string sobrenome, float valorPassagem, int numeroAssento, int vooId )
        {
            PassagemId = passagemId;
            CPF = cpf;
            Nome = nome;
            Sobrenome = sobrenome;
            ValorPassagem = valorPassagem;
            NumeroAssento = numeroAssento;
            VooId = vooId;
        }

        [Key]
        public int PassagemId { get; set; }

        [Required]
        [MaxLength(15)]
        public string CPF { get; set; }

        [Required]
        [MaxLength(20)]
        public  string Nome{ get; set; }

        [Required]
        [MaxLength(50)]
        public string Sobrenome { get; set; }

        [Required]
        public float ValorPassagem{ get; set; }

        [Required]
        public int NumeroAssento { get; set; }

        [Required]
        [ForeignKey("Voo")]
        public int VooId { get; set; }
        virtual public Voo Voo { get; set; }
    }
}
