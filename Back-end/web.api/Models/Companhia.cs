﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web.api.Models
{
    public class Companhia
    {
        public Companhia() { }

        public Companhia(int companhiaId, string nomeCompanhia)
        {
            CompanhiaId = companhiaId;
            NomeCompanhia = nomeCompanhia;
        }

        [Key]
        public int CompanhiaId { get; set; }
        [Required]
        [MaxLength(20)]
        public string NomeCompanhia { get; set; }

        virtual public ICollection<Voo> Voos { get; set; }

    }
}
