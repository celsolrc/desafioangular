﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace web.api.Models
{
    public class Voo
    {
        public Voo() {}
        public Voo(int vooId, int companhiaId, int numeroVoo, DateTime dataPartida, string horaPartida)
        {
            VooId = vooId;
            CompanhiaId = companhiaId;
            NumeroVoo = numeroVoo;
            DataPartida = dataPartida;
            HoraPartida = horaPartida;
        }

        [Key]
        public int VooId { get; set; }


        [Required]
        public int NumeroVoo { get; set; }

        [Required]
        public DateTime DataPartida { get; set; }

        [Required]
        [MaxLength(5)]
        public string HoraPartida { get; set; }

        [Required]
        public int CompanhiaId { get; set; }
        virtual public Companhia Companhia { get; set; }

        virtual public ICollection<Passagem> Passagens { get; set; }
    }
}