const PROXY_CONFIG = [
  {
    context: ["/api", "/api/*"],
    target: "http://localhost:52956",
    secure: false,
    changeOrigin: true
  }
];

module.exports = PROXY_CONFIG;
