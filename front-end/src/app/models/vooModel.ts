import { PassagemModel } from './passagemModel';

export class VooModel {
  companhiaId: number;
  vooId: number;
  numeroVoo: number;
  dataPartida: string;
  horaPartida: string;
  passagens: PassagemModel[];
}
