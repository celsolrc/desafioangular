export class PassagemModel {
  vooId: number;
  passagemId: number;
  cpf: string;
  nome: string;
  sobrenome: string;
  valorPassagem: number;
}
