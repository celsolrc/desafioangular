import { VooModel } from './vooModel';
export class CompanhiaModel {
  companhiaId: number;
  nomeCompanhia: string;
  voos: VooModel[];
}
