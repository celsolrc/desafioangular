import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';

import { LOCALE_ID } from '@angular/core';

import { AngularMaterialModule } from './angular-material.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PassagensComponent } from './components/passagens/passagens.component';

import { VoosService } from './services/voos.service';
import { PassagensService } from './services/passagens.service';
import { CompanhiasService } from './services/companhias.service';

import { PopupModule } from './popup/popup.module';
import { HomeComponent } from './components/home/home.component';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

registerLocaleData(localePt);
@NgModule({
  declarations: [
    AppComponent,
    PassagensComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AngularMaterialModule,
    PopupModule,
    FormsModule
  ],
  providers: [PassagensService, VoosService, CompanhiasService,
              {provide: LOCALE_ID, useValue: 'pt'} ],
  bootstrap: [AppComponent]
})
export class AppModule { }
