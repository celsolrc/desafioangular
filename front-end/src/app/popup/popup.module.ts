import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaskModule, IConfig } from 'ngx-mask';

import { FormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material.module';

import { CompanhiaPopupDialogComponent } from './companhia-popup-dialog/companhia-popup-dialog.component';
import { VooPopupDialogComponent } from './voo-popup-dialog/voo-popup-dialog.component';
import { GenericConfirmComponent } from './generic-confirm/generic-confirm.component';
import { PassagemPopupDialogComponent } from './passagem-popup-dialog/passagem-popup-dialog.component';

@NgModule({
  declarations: [
    CompanhiaPopupDialogComponent,
    VooPopupDialogComponent,
    GenericConfirmComponent,
    PassagemPopupDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AngularMaterialModule,
    NgxMaskModule.forRoot()
  ],
  providers: [],
})

export class PopupModule { }
