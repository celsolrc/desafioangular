import { PassagemModel } from './../models/passagemModel';
import { GenericConfirmComponent } from './generic-confirm/generic-confirm.component';
import { VooPopupDialogComponent } from './voo-popup-dialog/voo-popup-dialog.component';
import { PassagemPopupDialogComponent } from './passagem-popup-dialog/passagem-popup-dialog.component';
import { CompanhiaPopupDialogComponent } from './companhia-popup-dialog/companhia-popup-dialog.component';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { CompanhiaModel } from '../models/companhiaModel';
import { VooModel } from '../models/vooModel';

export enum ConfirmTypes {
  YESNO = 0,
  OKCANCEL = 1,
  OKCLOSE = 2,
  OK = 3,
  CANCEL = 4
}

@Injectable({
  providedIn: 'root'
})
export class PopupModalService {

  constructor( public dialog: MatDialog) {}

  private msgConfirm = [{ Yes: 'Sim', No: 'Não' }, { Yes: 'Ok', No: 'Cancelar' }, { Yes: 'Ok', No: 'Fechar' }];

  /***********************************************************************************
    Funcoes Utilitarias
  ********************************************/

  /* Retorna string com data no formato yyyy-MM-dd */
  formatDateStrYYYYMMDD(val: Date): string {
    return this.formatNumberStr(val.getFullYear(), 4) + '-' + this.formatNumberStr(val.getMonth() + 1, 2) + '-'
      + this.formatNumberStr(val.getDate(), 2);
  }

  /* Preenche com zeros a esquerda */
  formatNumberStr(val: number, nchar: number): string {
    const ret = ('0'.repeat(nchar) + val.toString());

    return ret.substring(ret.length - nchar, ret.length);
  }


  /***********************************************************************************
    Dialogos Popup
  ********************************************/
  /* Funcao generica para criar e apresentar o dialogo popup */
  openDialog(popup: any, modelData: any): Observable<any> {
    const dialogRef = this.dialog.open(popup, { width: '300px', data: modelData });

    return dialogRef.afterClosed();
  }

  /*
    Popups da Companhia
  */
  /* Cria e apresenta o dialogo de companhia */
  dialogoCompanhia(operacaoF: string, companhiaF: CompanhiaModel, readonlyF?: boolean): Observable<CompanhiaModel> {
    if (readonlyF === undefined) {
      readonlyF = false;
    }
    return this.openDialog(CompanhiaPopupDialogComponent,
      { operacao: operacaoF, companhia: companhiaF, readonly: readonlyF }) as Observable<CompanhiaModel>;
  }

  /* Cria e apresenta o dialogo de voo */
  dialogoVoo(operacaoF: string, vooF: VooModel, readonlyF?: boolean): Observable<VooModel> {
    const dataPartidaF = this.formatDateStrYYYYMMDD(new Date(vooF.dataPartida));
    if (readonlyF === undefined) {
      readonlyF = false;
    }

    return this.openDialog(VooPopupDialogComponent, {
      operacao: operacaoF, numeroVoo: vooF.numeroVoo,
      dataPartida: dataPartidaF, horaPartida: vooF.horaPartida,
      readonly: readonlyF
    }) as Observable<VooModel>;
  }

    /* Cria e apresenta o dialogo de passagem */
    dialogoPassagem(operacaoF: string, passagemF: PassagemModel, readonlyF?: boolean): Observable<PassagemModel> {
      if (readonlyF === undefined) {
        readonlyF = false;
      }

      return this.openDialog(PassagemPopupDialogComponent, {
        operacao: operacaoF, cpf: passagemF.cpf,
        nome: passagemF.nome, sobrenome: passagemF.sobrenome,
        valorPassagem: passagemF.valorPassagem,
        readonly: readonlyF
      }) as Observable<PassagemModel>;
    }

  /* Cria e apresenta o dialogo generico */
  dialogoGenerico(titulo: string, mensagem: string, msgBotaoYes?: string, msgBotaoNo?: string,
                  mostraNao?: boolean): Observable<boolean> {
    if (mostraNao === undefined) {
      mostraNao = true;
    }
    if (msgBotaoYes === undefined) {
      msgBotaoYes = 'Ok';
    }
    if (msgBotaoNo === undefined) {
      msgBotaoNo = 'Fechar';
    }

    return this.openDialog(GenericConfirmComponent, {
      titleBox: titulo, message: mensagem, buttonYesText: msgBotaoYes,
      buttonNoText: msgBotaoNo, showNo: mostraNao
    }) as Observable<boolean>;
  }

  /* Cria e apresenta o dialogo de confirmacao generica */
  dialogoConfirmacao(titulo: string, mensagem: string, confirmType?: ConfirmTypes): Observable<boolean> {

    if (confirmType === undefined) {
      confirmType = ConfirmTypes.YESNO;
    }

    return this.openDialog(GenericConfirmComponent, {
      titleBox: titulo, message: mensagem, showNo: ( (confirmType !== ConfirmTypes.OK) && (confirmType !== ConfirmTypes.CANCEL)),
      buttonNoText: this.msgConfirm[confirmType].No, buttonYesText: this.msgConfirm[confirmType].Yes
    }) as Observable<boolean>;
  }
}
