import { PassagemModel } from './../../models/passagemModel';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface PassagemDialogData {
  operacao: string;
  cpf: number;
  nome: string;
  sobrenome: string;
  valorPassagem: number;
  readonly: boolean;
}

@Component({
  selector: 'app-passagem-popup-dialog',
  templateUrl: './passagem-popup-dialog.component.html',
  styleUrls: ['./passagem-popup-dialog.component.css']
})
export class PassagemPopupDialogComponent {

  constructor( public dialogRef: MatDialogRef<PassagemPopupDialogComponent>,
               @Inject(MAT_DIALOG_DATA) public data: PassagemDialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
