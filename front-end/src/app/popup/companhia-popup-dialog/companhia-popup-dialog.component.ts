import { CompanhiaModel } from './../../models/companhiaModel';
import { Component, Inject } from '@angular/core';

import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface CompanhiaDialogData {
  operacao: string;
  companhia: CompanhiaModel;
  readonly: boolean;
}

@Component({
  selector: 'app-generic-popup-dialog',
  templateUrl: './companhia-popup-dialog.component.html',
  styleUrls: ['./companhia-popup-dialog.component.css']
})
export class CompanhiaPopupDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<CompanhiaPopupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CompanhiaDialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
