import { VooModel } from './../../models/vooModel';
import { Component, Inject } from '@angular/core';

import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Time } from '@angular/common';

export interface VooDialogData {
  operacao: string;
  numeroVoo: number;
  dataPartida: string;
  horaPartida: string;
  readonly: boolean;
}

@Component({
  selector: 'app-generic-popup-dialog',
  templateUrl: './voo-popup-dialog.component.html',
  styleUrls: ['./voo-popup-dialog.component.css']
})
export class VooPopupDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<VooPopupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VooDialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
