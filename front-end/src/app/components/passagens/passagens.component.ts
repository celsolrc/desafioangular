import { PopupModalService, ConfirmTypes } from './../../popup/popup-modal.service';
import { CompanhiaModel } from './../../models/companhiaModel';
import { CompanhiasService } from './../../services/companhias.service';
import { PassagemModel } from './../../models/passagemModel';
import { VooModel } from './../../models/vooModel';
import { Component, OnInit } from '@angular/core';
import { PassagensService } from '../../services/passagens.service';
import { VoosService } from '../../services/voos.service';

import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface PassagensNode {
  companhia?: CompanhiaModel;
  voo?: VooModel;
  passagem?: PassagemModel;
  level: number;
  name: string;
  children?: PassagensNode[];
}

@Component({
  selector: 'app-passagens',
  templateUrl: './passagens.component.html',
  styleUrls: ['./passagens.component.css']
})
export class PassagensComponent implements OnInit {

  treeControl = new NestedTreeControl<PassagensNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<PassagensNode>();

  TREE_DATA: PassagensNode[];

  constructor(private passagensService: PassagensService, private voosService: VoosService,
              private companhiasService: CompanhiasService,
              private popupModalService: PopupModalService) {
    this.loadTree();
  }

  public companhias: CompanhiaModel[];

  ngOnInit(): void {
  }


  /***********************************************************************************
    Funcoes para formatacao e exibicao
  ********************************************/
  /* Formata os dados do Voo */
  formataVoo(voo: VooModel): string {
    return voo.numeroVoo.toString() + ' - ' + this.formatDateStrDDMMYYYY(voo.dataPartida) + ' ' + voo.horaPartida;
  }

  /* Formata os dados da passagem */
  formataPassagem(passagem: PassagemModel): string {

    const valor = (passagem.valorPassagem).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });

    return '<p>' + passagem.sobrenome + ', ' + passagem.nome + ' - CPF: ' + passagem.cpf + ' - ' + valor + '</p>';
  }

  /* Seleciona, conforme o nivel do node, qual dado e formato deve ser apresentado no corpo do node */
  mostraConteudo(node: PassagensNode): string {
    let ret = '';

    if (node.level === 0) {
      ret = node.name;
    } else if (node.level === 1) {
      ret = this.formataVoo(node.voo);
    } else {
      ret = this.formataPassagem(node.passagem);
    }

    return ret;
  }
  /***********************************************************************************/

  /***********************************************************************************
    Funcoes Utilitarias
  ********************************************/
  /* Retorna string com data no formato dd-MM-yyyy-MM */
  formatDateStrDDMMYYYY(val: string): string {
    return val.substr(8, 2) + '/' + val.substr(5, 2) + '/' + val.substr(0, 4);
  }
  /***********************************************************************************/

  /***********************************************************************************
    Dialogos Popup
  ********************************************/
  /* Cria e apresenta o dialogo para consultar dados de uma companhia */
  dialogoConsultarCompanhia(node: PassagensNode): void {
    this.popupModalService.dialogoCompanhia('Consultar companhia', node.companhia, true).subscribe(result => {
      console.log('consultar companhia', result);
    });
  }

  /* Cria e apresenta o dialogo para editar dados de uma companhia */
  dialogoEditarCompanhia(node: PassagensNode): void {
    const oldCia = node.companhia.nomeCompanhia;

    this.popupModalService.dialogoCompanhia('Editar companhia', node.companhia).subscribe(result => {
      if (result === undefined) {
        node.companhia.nomeCompanhia = oldCia;

        console.log('Saiu sem gravar Companhia (Editar)');
      } else {
        console.log('editar companhia', result);
        node.companhia.nomeCompanhia = result.nomeCompanhia;

        this.companhiasService.update(node.companhia).subscribe(response => {
          console.log('Gravou ! alterou companhia', response);
          this.refreshTree();
        });
      }
    });
  }

  /* Cria e apresenta o dialogo para inserir uma nova companhia */
  dialogoInserirCompanhia(): void {
    const node = { level: 0, name: '', companhia: new CompanhiaModel() } as PassagensNode;

    this.popupModalService.dialogoCompanhia('Inserir companhia', node.companhia).subscribe(result => {
      if (result === undefined) {
        console.log('Saiu sem gravar Companhia (Inserir)');
      } else {
        const novaCompanhia = { companhiaId: 0, nomeCompanhia: result.nomeCompanhia } as CompanhiaModel;
        console.log('inserir companhia', novaCompanhia);

        this.companhiasService.create(novaCompanhia).subscribe(response => {
          console.log('Gravou ! inserir companhia', response);

          // Manter a lista em memoria para evitar carregar do banco
          this.companhias.push(novaCompanhia);
          this.refreshTree();
        });
      }
    });
  }

  /* Cria e apresenta o dialogo para excluir uma companhia */
  dialogoExcluirCompanhia(node: PassagensNode): void {

    this.popupModalService.dialogoConfirmacao('Excluir companhia', 'Deseja excluir a companhia ?', ConfirmTypes.YESNO)
      .subscribe(result => {
        if (result) {
          console.log('excluir companhia', result);

          this.companhiasService.delete(node.companhia.companhiaId).subscribe(response => {
            console.log('Excluir ! excluiu companhia', response);
            this.loadTree();
          });
        }
      });
  }

  /*
    Popups do Voo
  */
  /* Cria e apresenta o dialogo para consultar os dados de um voo */
  dialogoConsultarVoo(node: PassagensNode): void {
    this.popupModalService.dialogoVoo('Consultar voo', node.voo, true).subscribe(result => {
      console.log('consultar voo', result);
    });
  }

  /* Cria e apresenta o dialogo para editar dados de um voo */
  dialogoEditarVoo(node: PassagensNode): void {
    const oldVoo = JSON.parse(JSON.stringify(node.voo)) as VooModel;

    this.popupModalService.dialogoVoo('Editar voo', node.voo).subscribe(result => {
      if (result === undefined) {
        node.voo.numeroVoo = oldVoo.numeroVoo;
        node.voo.dataPartida = oldVoo.dataPartida;
        node.voo.horaPartida = oldVoo.horaPartida;

        console.log('Saiu sem gravar Voo (Editar)');
      } else {

        console.log('editar voo', result);

        node.voo.numeroVoo = result.numeroVoo;
        node.voo.dataPartida = result.dataPartida;
        node.voo.horaPartida = result.horaPartida;

        this.voosService.update(node.voo).subscribe(response => {
          console.log('Gravou ! alterou voo', response);
          this.refreshTree();
        });
      }
    });
  }

  /* Cria e apresenta o dialogo para inserir um novo voo */
  dialogoInserirVoo(node: PassagensNode): void {
    let novoVoo = {
      companhiaId: node.companhia.companhiaId, vooId: 0,
      dataPartida: (new Date(Date.now())).toString()
    } as VooModel;

    this.popupModalService.dialogoVoo('Inserir voo', novoVoo).subscribe(result => {

      if (result === undefined) {
        console.log('Saiu sem gravar Voo (Inserir)');
      } else {

        novoVoo = {
          companhiaId: node.companhia.companhiaId,
          vooId: 0,
          dataPartida: result.dataPartida,
          horaPartida: result.horaPartida,
          numeroVoo: result.numeroVoo
        } as VooModel;

        console.log('inserir voo', novoVoo);

        this.voosService.create(novoVoo).subscribe(response => {
          console.log('Gravou ! inserir voo', response);
          // Manter a lista em memoria para evitar carregar do banco
          node.companhia.voos.push(novoVoo);
          this.refreshTree();
        });

      }
    });
  }

  /* Cria e apresenta o dialogo para excluir um voo */
  dialogoExcluirVoo(node: PassagensNode): void {

    this.popupModalService.dialogoConfirmacao('Excluir voo', 'Deseja excluir o voo ?', ConfirmTypes.YESNO)
      .subscribe(result => {
        if (result) {
          console.log('excluir voo', result);

          this.voosService.delete(node.voo.vooId).subscribe(response => {
            console.log('Excluir ! excluiu voo', response);
            this.loadTree();
          });
        }
      });
  }

  /*
    Popups da Passagem
  */
  /* Cria e apresenta o dialogo para consultar dados de uma passagem */
  dialogoConsultarPassagem(node: PassagensNode): void {
    this.popupModalService.dialogoPassagem('Consultar passagem', node.passagem, true).subscribe(result => {
      console.log('consultar passagem', result);
    });
  }

  /* Cria e apresenta o dialogo para editar dados de uma passagem */
  dialogoEditarPassagem(node: PassagensNode): void {
    const oldPassagem = JSON.parse(JSON.stringify(node.passagem)) as PassagemModel;

    this.popupModalService.dialogoPassagem('Editar passagem', node.passagem).subscribe(result => {
      if (result === undefined) {
        node.passagem.cpf = oldPassagem.cpf;
        node.passagem.nome = oldPassagem.nome;
        node.passagem.sobrenome = oldPassagem.sobrenome;
        node.passagem.valorPassagem = oldPassagem.valorPassagem;

        console.log('Saiu sem gravar Passagem (Editar)');
      } else {

        console.log('editar passagem', result);

        node.passagem.cpf = result.cpf;
        node.passagem.nome = result.nome;
        node.passagem.sobrenome = result.sobrenome;
        node.passagem.valorPassagem = result.valorPassagem;

        this.passagensService.update(node.passagem).subscribe(response => {
          console.log('Gravou ! alterou passagem', response);
          this.refreshTree();
        });
      }
    });
  }

  /* Cria e apresenta o dialogo para inserir uma nova passagem */
  dialogoInserirPassagem(node: PassagensNode): void {
    let novaPassagem = {
      vooId: node.voo.vooId, passagemId: 0,
    } as PassagemModel;

    this.popupModalService.dialogoPassagem('Inserir passagem', novaPassagem).subscribe(result => {

      if (result === undefined) {
        console.log('Saiu sem gravar Passagem (Inserir)');
      } else {

        novaPassagem = {
          vooId: node.voo.vooId, passagemId: 0,
          cpf: result.cpf,
          nome: result.nome,
          sobrenome: result.sobrenome,
          valorPassagem: result.valorPassagem
        } as PassagemModel;

        console.log('inserir passagem', novaPassagem);

        this.passagensService.create(novaPassagem).subscribe(response => {
          console.log('Gravou ! inserir passagem', response);
          // Manter a lista em memoria para evitar carregar do banco
          node.voo.passagens.push(novaPassagem);
          this.refreshTree();
        });
      }
    });
  }

  /* Cria e apresenta o dialogo para excluir uma passagem */
  dialogoExcluirPassagem(node: PassagensNode): void {

    this.popupModalService.dialogoConfirmacao('Excluir passagem', 'Deseja excluir a passagem ?', ConfirmTypes.YESNO)
      .subscribe(result => {
        if (result) {
          console.log('excluir passagem', result);

          this.passagensService.delete(node.passagem.passagemId).subscribe(response => {
            console.log('Excluir ! excluiu passagem', response);
            this.loadTree();
          });
        }
      });
  }

  /***********************************************************************************
    Manipulacao dos eventos dos Nodes da Tree, encaminhando conforme o
    level (0=compania, 1=voo e 2 =passagem) e o tipo de evento
    (edicao, adicao e remocao)
  ********************************************/
  /* Identifica e encaminha a edicao de um node conforme o level (0=compania, 1=voo e 2 =passagem) */
  editNode(node: PassagensNode): void {
    if (node.level === 0) {
      this.dialogoEditarCompanhia(node);
    } else if (node.level === 1) {
      this.dialogoEditarVoo(node);
    } else if (node.level === 2) {
      this.dialogoEditarPassagem(node);
    }
  }

  /* Identifica e encaminha a adicao de um node conforme o level (0=compania, 1=voo e 2 =passagem) */
  addNode(node?: PassagensNode): void {
    if ((node === null) || (node === undefined) || (node.level === undefined) || (node.level === null)) {
      this.dialogoInserirCompanhia();
    } else if (node.level === 0) {
      this.dialogoInserirVoo(node);
    } else if (node.level === 1) {
      this.dialogoInserirPassagem(node);
    }
  }

  /* Identifica e encaminha a edicao de um node conforme o level (0=compania, 1=voo e 2 =passagem) */
  consultaNode(node: PassagensNode): void {
    if (node.level === 0) {
      this.dialogoConsultarCompanhia(node);
    } else if (node.level === 1) {
      this.dialogoConsultarVoo(node);
    } else if (node.level === 2) {
      this.dialogoConsultarPassagem(node);
    }
  }

  /* Identifica e encaminha a remocao de um node conforme o level (0=compania, 1=voo e 2 =passagem) */
  deleteNode(node: PassagensNode): void {
    if (node.level === 0) {
      this.dialogoExcluirCompanhia(node);
    } else if (node.level === 1) {
      this.dialogoExcluirVoo(node);
    } else if (node.level === 2) {
      this.dialogoExcluirPassagem(node);
    }
  }
  /***********************************************************************************/

  /***********************************************************************************
    Montagem dos Nodes da Tree
  ********************************************/
  /* Insere os dados das passagens na tree com o level 2 */
  nodeInserirPassagens(passagemNova: PassagemModel[]): PassagensNode[] {
    const nodes = new Array<PassagensNode>();

    if ((passagemNova !== undefined) && (passagemNova.length > 0)) {
      passagemNova.forEach(itemPassagem => {
        const node = {
          passagem: itemPassagem,
          name: itemPassagem.nome,
          level: 2,
          children: []
        } as PassagensNode;

        nodes.push(node);
      });
    }

    return nodes;
  }

  /* Insere os dados dos voos na Tree com o level 1,
     chamado o nodeInserirPassagens quando houverem */
  nodeInserirVoos(voosNovos: VooModel[]): PassagensNode[] {
    const nodes = new Array<PassagensNode>();

    if ((voosNovos !== undefined) && (voosNovos.length > 0)) {
      voosNovos.forEach(vooNovo => {
        const node = {
          voo: vooNovo,
          name: vooNovo.numeroVoo.toString(),
          level: 1,
          children: this.nodeInserirPassagens(vooNovo.passagens)
        };

        nodes.push(node);
      });
    }

    return nodes;
  }

  /* Insere os dados das companhias na Tree com o level 0,
     chamado o nodeInserirVoos quando houverem */
  nodeInserirCompanhia(companhiaNova: CompanhiaModel): PassagensNode {
    let node: PassagensNode;

    if (companhiaNova !== undefined) {
      node = {
        companhia: companhiaNova,
        name: companhiaNova.nomeCompanhia,
        level: 0,
        children: this.nodeInserirVoos(companhiaNova.voos)
      };
    }

    return node;
  }

  /* Atualiza a tree com o dados */
  refreshTree(): void {
    this.TREE_DATA = [];

    this.companhias.forEach(companhia => {
      this.TREE_DATA.push(this.nodeInserirCompanhia(companhia));
    });

    this.dataSource.data = this.TREE_DATA;
  }

  /* Carrega os dados do Banco e chama refreshTree() */
  loadTree(): void {
    this.TREE_DATA = [];

    this.companhiasService.get().subscribe(retorno => {
      this.companhias = retorno;

      this.refreshTree();
    });
  }
  /***********************************************************************************/

  /* Flag utilizada pela tree para indicar se tem no filho */
  hasChild = (_: number, node: PassagensNode) => !!node.children && node.children.length > 0;
}

