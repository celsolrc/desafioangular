import { PassagemModel } from '../models/passagemModel';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseCrudHttpService } from './base-crud-http.service';

@Injectable({
  providedIn: 'root'
})
export class PassagensService  extends BaseCrudHttpService<PassagemModel> {

  constructor(private http1: HttpClient)
  {
    super(http1, 'passagens');
  }

  getId(model: PassagemModel): number {
    return model.passagemId;
  }

}
