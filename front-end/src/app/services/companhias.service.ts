import { CompanhiaModel } from '../models/companhiaModel';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseCrudHttpService } from './base-crud-http.service';
import { NgModel } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CompanhiasService extends BaseCrudHttpService<CompanhiaModel> {

  constructor(private http1: HttpClient) {
    super(http1, 'companhias');
  }

  getId(model: CompanhiaModel): number {
    return model.companhiaId;
  }
}
