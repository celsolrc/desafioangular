import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export abstract class BaseCrudHttpService<T> {

  private readonly API = environment.apiUrl;
  private APIController: string;

  constructor(private http: HttpClient, api: string) {
    this.APIController = api;
  }

  find(Id: number): Observable<T> {
    return this.http.get<T>(`${this.API}${this.APIController}/${Id}`,
    { headers: this.getHeader()});
  }

  get(): Observable<T[]> {
    return this.http.get<T[]>(`${this.API}${this.APIController}`,
    { headers: this.getHeader()});
  }

  create(newModel: T): Observable<any> {
    return this.http.post(`${this.API}${this.APIController}`, newModel,
    { headers: this.getHeader()});
  }

  abstract getId(model: T): number;

  update(model: T): Observable<any> {
    return this.http.put(`${this.API}${this.APIController}/${this.getId(model)}`, model,
    { headers: this.getHeader()});
  }

  delete(Id: number): Observable<any> {
    return this.http.delete(`${this.API}${this.APIController}/${Id}`,
    { headers: this.getHeader()});
  }

  getHeader(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

}
