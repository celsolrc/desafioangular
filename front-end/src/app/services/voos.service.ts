import { VooModel } from './../models/vooModel';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseCrudHttpService } from './base-crud-http.service';

@Injectable({
  providedIn: 'root'
})
export class VoosService extends BaseCrudHttpService<VooModel> {

  constructor(private http1: HttpClient) {
    super(http1, 'voos');
  }

  getId(model: VooModel): number {
    return model.vooId;
  }
}
